package com.example.redishandRabbitMq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class RedishRabbitMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedishRabbitMqApplication.class, args);
	}

}
