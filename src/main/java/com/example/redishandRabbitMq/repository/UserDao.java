package com.example.redishandRabbitMq.repository;


import com.example.redishandRabbitMq.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User,Long> {
}