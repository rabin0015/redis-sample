package com.example.redishandRabbitMq.service;

import lombok.RequiredArgsConstructor;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RedisOperationsMaster<T> {
    private final RedisTemplate<String, List<T>> redisTemplate;

    public void saveToCache(List<T> data, String cacheName, String keyName) {
        redisTemplate.opsForHash().put(cacheName, keyName, data);
    }

    public List<T> findByCacheAndKey(String cacheName, String key) {
        return (List<T>) redisTemplate.opsForHash().get(cacheName, key);
    }

    public void deleteByCacheName(String cacheName) {
        redisTemplate.delete(cacheName);
    }

    public void deleteByKey(String cacheName, String key) {
        redisTemplate.opsForHash().delete(cacheName, key);
    }

    public Map<Object, Object> getHashDataByCacheKey(String cacheKeyName) {
        return redisTemplate.opsForHash().entries(cacheKeyName);
    }
}
