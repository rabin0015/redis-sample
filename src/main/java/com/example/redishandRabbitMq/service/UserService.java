package com.example.redishandRabbitMq.service;
import com.example.redishandRabbitMq.entity.User;

import java.util.List;

public interface UserService {

    void saveUser(User user);

    List<User> fetchAllUser();

    User fetchUserById(Long id);

    void deleteUser(Long id);
}