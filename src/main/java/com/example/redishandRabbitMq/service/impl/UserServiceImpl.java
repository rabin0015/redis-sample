package com.example.redishandRabbitMq.service.impl;


import com.example.redishandRabbitMq.entity.User;
import com.example.redishandRabbitMq.repository.UserDao;
import com.example.redishandRabbitMq.service.RedisOperationsMaster;
import com.example.redishandRabbitMq.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private static final String KEY = "USER";
    private static final String CACHE_NAME="USER_LIST";
    private static final String CACHE_UPDATE_FAILURE = "Unable to update cache";

    private final UserDao userDao;
    private final RedisOperationsMaster<User> redisOperationsMaster;

    public void redisItemDelete() {
        try {
            redisOperationsMaster.deleteByCacheName(CACHE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(CACHE_UPDATE_FAILURE);
        }
    }

    @Override
    public void saveUser(User user) {
        userDao.save(user);
        // Remove from redis to update data
        redisItemDelete();
    }

    @Override
    public List<User> fetchAllUser() {
        /*
         * Check if available in redis cache
         * If present, return from cache else fetch from db.
         */
        List<User> userList = redisOperationsMaster.findByCacheAndKey(CACHE_NAME,KEY);
        if (!Objects.isNull(userList) && !userList.isEmpty()) {
            return userList;
        }

        List<User> users = userDao.findAll();
        /*
         * Before returning data, save to redis cache for next fetch.
         */
        redisOperationsMaster.saveToCache(userDao.findAll(),CACHE_NAME,KEY);
        return users;

    }

    @Override
    public User fetchUserById(Long id) {
        for (User user : userDao.findAll()) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void deleteUser(Long id) {
        User user = userDao.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
        userDao.deleteById(user.getId());

    }
}